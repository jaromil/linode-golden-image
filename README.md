# Base creation of a Devuan image to use on Linode

This setup allows the use of Devuan on Linode VMs to be installed as "golden image", which is a native format for Linode virtual machines that grants very fast deploy times. It supports Devuan 4 "Chimaera" (equivalent to Debian 11 "Bullseye").

## Requirements

Install Packer and the Linode extension following these instructions: https://www.linode.com/docs/guides/how-to-use-linode-packer-builder/

Install Ansible, the packaged one on most systems works, just `apt-get install ansible`.

## Getting started

Get an API Token from your current Linode account and `export TOKEN=your_secret_api_token_hex`

Optionally tweak the file `devuan.pkr.hcl` and check if valid with `packer validate -var linode_api_token=$TOKEN devuan.pkr.hcl`

Optionally tweak the `install-devuan.yaml` ansible playbook and set your root password (default is `devuan`)

Initialize with `packer init devuan.pkr.hcl`

Build with `packer build -var linode_api_token=$TOKEN devuan.pkr.hcl`

Wait a bit and watch the packer logs

## Internals

These are the main components:

- Cloud command setup on Linode: [packer builder](devuan.pkr.hcl)
- Devuan install via ansible: [ansible provisioning](install-devuan.yaml)

## Credits

Scripts by Jaromil @ Dyne.org

This is all released as Public Domain
