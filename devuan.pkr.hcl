packer {
  required_plugins {
    linode = {
      version = ">= 1.0.2"
      source  = "github.com/linode/linode"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "linode" "devuan" {
  image             = "linode/debian11"
  image_description = "Linode golden image based on Devuan"
  image_label       = "devuan"
  instance_label    = "devuan-image-${local.timestamp}"
  instance_type     = "g6-nanode-1"
  linode_token	    = "${var.linode_api_token}"
  region            = "eu-central"
  ssh_username      = "root"
}

variable "ansible_host" { default = "devuan" }
variable "linode_api_token" {
  type    = string
  default = ""
}

build {
  sources = ["source.linode.devuan"]
  provisioner "ansible" {
    playbook_file = "./install-devuan.yaml"
  }
}
